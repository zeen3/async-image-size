# async-image-size

entirely async promise-based image-size

based off typescript and buffers and the like

requires DataView access

## Usage

ES2018+

```js
import imageSize from 'async-image-size'
import {promises as fs} from 'fs'
fs.readFile('/path/to/some/file.png').then(imageSize).then(sizes => {
	for (const [width, height, depth = 0] of sizes)
		console.log('file is %d * %d (* %d)', width, height, depth)
})
```

ES5 (and below)

```js
const imageSize = require('async-image-size').imageSize
const fs = require('fs')
fs.readFile('/path/to/some/file.png', function (err, buf) {
	if (err) throw err
	imageSize(buf).then(function (sizes) {
		for (var i = 0; i < sizes.length; ++i) console.log('file is %d * %d (* %d)', sizes[i][0], sizes[i][1], sizes[i][2])
	})
})
```

Functions with svg, ico, bmp, icns, tiff, webp, dds, png, jpg, eps, ktx and so on.

Relies heavily on magic numbers for reading information.

## Tests

Currently no real tests are existing; only really testing that there aren't errors in the loader rn.


```shell
$ npm test
# ........
$ npm run test-esm
[Module] { ....... }
```
