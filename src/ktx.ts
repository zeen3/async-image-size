import {imageInfoReader, isReader } from './check'

export const ktx: Readonly<imageInfoReader> = Object.freeze({
	[isReader]: true,
	min: 48,
	// «KTX 11»\r\n\x1A\n
	is: b => b.getUint32(0) === 0xAB_4B_54_58 &&
		b.getUint32(4) === 0x20_31_31_BB &&
		b.getUint32(8) === 0x0D_0A_1A_0A,
	size(b) {
		const le = b.getUint32(12) === 0x01_02_03_04
		return [[
			b.getUint32(40, le),
			b.getUint32(44, le),
			b.getUint32(48, le)
		]]
	}
} as imageInfoReader)
export default ktx
