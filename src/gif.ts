import {imageInfoReader, isReader } from './check'

export const gif: Readonly<imageInfoReader> = Object.freeze({
	[isReader]: true,
	min: 10,
	// 0..4 === 'GIF8'
	// 5..6 === '7a' || 5..6 === '9a'
	is: b => b.getUint32(0) === 0x47_49_46_38
		&& b.getUint16(4) === 0x37_61
		|| b.getUint16(4) === 0x39_61,
	size: b => [[
		b.getUint16(6, true),
		b.getUint16(8, true)
	]]
} as imageInfoReader)
export default gif
