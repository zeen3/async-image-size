import {imageInfoReader, isReader } from './check'

export const psd: Readonly<imageInfoReader> = Object.freeze({
	[isReader]: true,
	min: 18,
	// 8BPS
	is: b => b.getUint32(0) === 0x38_42_50_53,
	size: b => [[
		b.getUint32(18),
		b.getUint32(14)
	]]
} as imageInfoReader)
export default psd
