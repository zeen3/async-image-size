import { imageInfoReader, isReader } from './check'
export const jpg: imageInfoReader = Object.freeze({
	[isReader]: true,
	min: 4096,
	is(buf) {return buf.getUint16(0) === 0xFF_D8},
	size(buf) {
		let offset = 4, i = 0, next = 0
		while (offset < buf.byteLength) {
			i = buf.getUint16(offset)
			next = buf.getUint8(i + 1)
			switch (next) {
				case 0xC0: case 0xC1: case 0xC2:
					return [[
						buf.getUint16(i + 5),
						buf.getUint16(i + 7)
					]]
				default:
					offset = i
			}
		}

		throw new TypeError('invalid jpeg, no size retrievable')
	}
} as imageInfoReader)
export default jpg
