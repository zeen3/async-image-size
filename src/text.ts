let txd: TextDecoder;
// textdecoder, but not really
try {
	if ('TextDecoder' in self)
		txd = new TextDecoder;
} catch (e) {
	txd = ({
		decode(x: BufferSource) {
			return ArrayBuffer.isView(x) ?
				String.fromCharCode(...new Uint8Array(x.buffer, x.byteOffset, x.byteLength)) :
				String.fromCharCode(...new Uint8Array(x));
		},
		encoding: 'latin1',
		fatal: false,
		ignoreBOM: false
	});
};
export {txd}
