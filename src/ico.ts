import {imageInfoReader, isReader } from './check'
export const ico: imageInfoReader = Object.freeze({
	[isReader]: true,
	min: 2048,
	// little endian encoding, view is actually 00 00 01 00
	is(b) {
		const val = b.getUint32(0, true)
		return (0x01_00_00 === val || 0x02_00_00 === val)
	},
	size(b) {
		const res = [],
			num = b.getUint16(4, true)
		let i = 0
		do {
			const off = 6 + (i << 4)
			res.push([
				b.getUint8(off) || 256,
				b.getUint8(off + 1) || 256
			])
		} while (++i < num);
		return res
	}
} as imageInfoReader)
export default ico
