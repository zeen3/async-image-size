import {imageInfoReader, isReader } from './check'

export const tiff: Readonly<imageInfoReader> = Object.freeze({
	// based on https://github.com/image-size/image-size/blob/master/lib/types/tiff.js
	[isReader]: true,
	min: Infinity,
	is(b) {
		const val = b.getUint32(0)
		return val === 0x49_49_2A_00 ||
			val === 0x4D_4D_00_2A
	},
	size(b) {
		// test endianess
		const le = b.getUint16(0) === 0x49_49,
			tags: number[] = []
		// offset (should try ensuring this can be loaded first tho...)
		let off = b.getUint32(4, le)
		if (off > (b.byteLength - 1024))
			off = b.byteLength - off - 8
		do {
			// code = key
			// ty = type of info
			// ln = length
			const code = b.getUint16(0 + off, le),
				ty = b.getUint16(2 + off, le),
				ln = b.getUint32(4 + off, le)
			if (code === 0) break // eof
			if (ln === 1 && (ty === 3 || ty === 4))
				tags[code] = (b.getUint16(off + 8,
					le) << 16) |
					b.getUint16(off + 10, le);
			off += 12
		} while (off < b.byteLength);
		const w = tags[0x01_00], h = tags[0x01_01]
		if (w === undefined || h === undefined)
			throw new TypeError('invalid tiff, no size retrievable')
		return [[w, h]]
	}
} as imageInfoReader)
export default tiff
