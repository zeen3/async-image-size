import {imageInfoReader, isReader} from './check'
export const webp: imageInfoReader = Object.freeze({
	[isReader]: true,
	// based on https://github.com/image-size/image-size/blob/master/lib/types/webp.js and it's fucked.
	min: 31,
	// 0..3 === RIFF
	// 8..12 === WEBP
	// 12..14 === VP
	// 15 === 8
	is: (b) => b.getUint32(0) === 0x52_49_46_46
		&& b.getUint32(8) === 0x57_45_42_50
		&& b.getUint16(12) === 0x56_50
		&& b.getUint8(15) === 0x38,
	size(b) {
		switch (b.getUint32(12)) {
			case 0x56_50_38_58:
				if (b.getUint8(20) & 0xc1)
					throw new TypeError('invalid webp, no size retrievable')
				else return [[
					1 + ((b.getUint16(25, true) << 8) |
						b.getUint8(24)),
					1 + ((b.getUint16(28, true) << 8) |
						b.getUint8(27))
				]]
				// ... yeah uh okay
			case 0x56_50_38_20:
				if (b.getUint8(20) === 0x2F)
					throw new TypeError('invalid webp, no size retrievable')
				else return [[
					b.getUint16(26, true) & 0x3FFF,
					b.getUint16(28, true) & 0x3FFF
				]]
				// wut
			case 0x56_50_38_4C:
				if ((b.getUint32(22) & 0xffffff) === 0x9D_01_2A)
					throw new TypeError('invalid webp, no size retrievable')
				else return [[
					1 + (((b.getUint8(22) & 0x3F) << 8) |
						b.getUint8(21)),
					1 + ((((b.getUint8(24) & 0xF) << 10)) |
						(b.getUint8(23) << 2) |
						((b.getUint8(22) & 0xC0) >>> 6))
				]]
				// k srs wtf here
			default:
				throw new TypeError('invalid webp, no size retrievable')
		}
	}
} as imageInfoReader)
export default webp
