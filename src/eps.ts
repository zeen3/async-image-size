import {imageInfoReader, isReader } from './check'
import {txd} from './text'

const gBoundingBox = /^%%BoundingBox:[\t ]*(\d+)[\t ]+(\d+)[\t ]+(\d+)[\t ]+(\d+)[\t ]*$/m
export const eps: Readonly<imageInfoReader> = Object.freeze({
	[isReader]: true,
	min: 64,
	is: b => b.getUint32(0) === 0x25_21_50_53 ||
		b.getUint32(0) === 0xC5_D0_D3_C6,
	size(b) {
		const s = txd.decode(b)
		const bo = s.match(gBoundingBox)
		if (!bo) throw new TypeError('Not an encapsulated postscript file holding boundingbox information')
		return [[
			parseInt(bo[3], 10) - parseInt(bo[1], 10),
			parseInt(bo[4], 10) - parseInt(bo[2], 10)
		]]
	}
} as imageInfoReader)
export default eps
