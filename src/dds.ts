import {imageInfoReader, isReader } from './check'
export const dds: Readonly<imageInfoReader> = Object.freeze({
	[isReader]: true,
	min: 20,
	// DDS [space]
	is: b => b.getUint32(0) === 0x44_44_53_20,
	size: b => [[
		b.getUint32(12, true),
		b.getUint32(16, true)
	]]
} as imageInfoReader)
export default dds
