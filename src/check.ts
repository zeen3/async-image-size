export const isReader = Symbol('is image info reader')
export interface imageInfoReader {
	[isReader]: true;
	readonly min: number;
	is(buf: DataView): boolean;
	size(buf: DataView): ReadonlyArray<ReadonlyArray<number>>;
}
export type iirp = Promise<imageInfoReader> | Promise<Readonly<imageInfoReader>>;
const mods = new WeakMap<any, imageInfoReader>();
const getDefault = async (mod:any):Promise<imageInfoReader> => {
	const mmod = mods.get(mod)
	if (mmod && isReader in mmod) return mmod
	else if (Object.isFrozen(mod.default) && isReader in mod.default) {
		mods.set(mod, mod.default)
		return mod.default
	}
	throw new TypeError('not an image info reader module')
}


export interface imageInfo {
	readonly tiff: iirp;
	readonly icns: iirp;
	readonly webp: iirp;
	readonly jpeg: iirp;
	readonly apng: iirp;
	readonly bmp: iirp;
	readonly dds: iirp;
	readonly ico: iirp;
	readonly cur: iirp;
	readonly jpg: iirp;
	readonly png: iirp;
	readonly gif: iirp;
	readonly svg: iirp;
	readonly ktx: iirp;
	readonly eps: iirp;
	readonly [k: string]: iirp | undefined
}
const infos: Readonly<imageInfo> = Object.freeze({
	get tiff() {return import('./tiff').then(getDefault)},
	get icns() {return import('./icns').then(getDefault)},
	get webp() {return import('./webp').then(getDefault)},
	get jpeg() {return import('./jpg').then(getDefault)},
	get apng() {return import('./png').then(getDefault)},
	get ktx() {return import('./ktx').then(getDefault)},
	get dds() {return import('./dds').then(getDefault)},
	get bmp() {return import('./bmp').then(getDefault)},
	get ico() {return import('./ico').then(getDefault)},
	get cur() {return import('./ico').then(getDefault)},
	get jpg() {return import('./jpg').then(getDefault)},
	get png() {return import('./png').then(getDefault)},
	get gif() {return import('./gif').then(getDefault)},
	get svg() {return import('./svg').then(getDefault)},
	get eps() {return import('./eps').then(getDefault)},
})
const imageInfoFirstBytes: Readonly<{readonly [k: number]: iirp;}> = Object.freeze({
	get [0x00_00]() {return infos.ico},
	get [0x0A_0A]() {return infos.svg},
	get [0x0A_3C]() {return infos.svg},
	get [0x25_21]() {return infos.eps},
	get [0x3C_3F]() {return infos.svg},
	get [0x42_4D]() {return infos.bmp},
	get [0x44_44]() {return infos.dds},
	get [0x47_49]() {return infos.gif},
	get [0x49_43]() {return infos.icns},
	get [0x49_49]() {return infos.tiff},
	get [0x4D_4D]() {return infos.tiff},
	get [0x52_49]() {return infos.webp},
	get [0x68_38]() {return infos.icns},
	get [0x69_63]() {return infos.icns},
	get [0x69_68]() {return infos.icns},
	get [0x69_6C]() {return infos.icns},
	get [0x69_73]() {return infos.icns},
	get [0x69_74]() {return infos.icns},
	get [0x6C_38]() {return infos.icns},
	get [0x73_38]() {return infos.icns},
	get [0x74_38]() {return infos.icns},
	get [0x89_50]() {return infos.png},
	get [0xAB_4B]() {return infos.ktx},
	get [0xC5_D0]() {return infos.eps},
	get [0xFF_D8]() {return infos.jpg},
})

export const imageInfoResolver = async (buf: ArrayBufferView, off = 0): Promise<ReadonlyArray<ReadonlyArray<number>>> => {
	const u16 = new DataView(buf.buffer, buf.byteOffset + off, 2)
		.getUint16(0)
	if (!(u16 in imageInfoFirstBytes)) throw new SyntaxError('magic bytes not found in first bytes')
	const info = await imageInfoFirstBytes[u16]
	const dv = new DataView(buf.buffer, buf.byteOffset + off,
		Math.min(info.min,
			buf.buffer.byteLength - (buf.byteOffset + off)))
	if (info.is(dv)) return info.size(dv)
	throw new TypeError('magic bytes were incorrect or lacking length')
}

export {
	imageInfoResolver as default,
	imageInfoResolver as imageSize,
	infos as imageContainerInfo,
	infos,
	imageInfoFirstBytes,
	imageInfoFirstBytes as magic,
}
