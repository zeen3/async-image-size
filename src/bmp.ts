import {imageInfoReader, isReader } from './check'

export const bmp: Readonly<imageInfoReader> = Object.freeze({
	[isReader]: true,
	min: 26,
	// BM
	is: b => b.getUint16(0) === 0x42_4D,
	size: b => [[
		b.getUint32(18, true),
		Math.abs(b.getInt32(22, true))
	]]
} as imageInfoReader)
export default bmp
