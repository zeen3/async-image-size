import { imageInfoReader, isReader } from './check'
export const png: imageInfoReader = Object.freeze({
	[isReader]: true,
	min: 40,
	// 0..8 === '\x89png\r\n\x1a\n'
	is: b => b.getUint32(0) === 0x89_50_4e_47
		&& b.getUint32(4) === 0x0d_0a_1a_0a
		// 12..16 === 'ihdr'
		// ? true
		// : 12..16 === 'cgbi' && 28..32 === 'ihdr'
		&& (
			b.getUint32(12) === 0x49_48_44_52
			? true
			: b.getUint32(12) === 0x43_67_42_49
			&& b.getUint32(28) === 0x49_48_44_52
		),
	size: buf => buf.getUint32(12) === 0x43_67_42_49
		? [[buf.getUint32(16), buf.getUint32(20)]]
		: [[buf.getUint32(32), buf.getUint32(36)]]
} as imageInfoReader)
export default png
