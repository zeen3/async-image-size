import {imageInfoReader, isReader } from './check'

const types: Readonly<{[key: number]: number}> = Object.freeze({
	[0x49_43_4F_4E]: 32, // ICON
	[0x49_43_4E_23]: 32, // ICN#
	[0x69_63_6D_23]: 16, // icm#
	[0x69_63_6D_34]: 16, // icm4
	[0x69_63_6D_38]: 16, // icm8
	[0x69_63_73_23]: 16, // ics#
	[0x69_63_73_34]: 16, // ics4
	[0x69_63_73_38]: 16, // ics8
	[0x69_73_33_32]: 16, // is32
	[0x73_38_6D_6B]: 16, // s8mk
	[0x69_63_70_34]: 16, // icp4
	[0x69_63_6C_34]: 32, // icl4
	[0x69_63_6C_38]: 32, // icl8
	[0x69_6C_33_32]: 32, // il32
	[0x6C_38_6D_6B]: 32, // l8mk
	[0x69_63_70_35]: 32, // icp5
	[0x69_63_31_31]: 32, // ic11
	[0x69_63_68_34]: 48, // ich4
	[0x69_63_68_38]: 48, // ich8
	[0x69_68_33_32]: 48, // ih32
	[0x68_38_6D_6B]: 48, // h8mk
	[0x69_63_70_36]: 64, // icp6
	[0x69_63_31_32]: 32, // ic12
	[0x69_74_33_32]: 128, // it32
	[0x74_38_6D_6B]: 128, // t8mk
	[0x69_63_30_37]: 128, // ic07
	[0x69_63_30_38]: 256, // ic08
	[0x69_63_31_33]: 256, // ic13
	[0x69_63_30_39]: 512, // ic09
	[0x69_63_31_34]: 512, // ic14
	[0x69_63_31_30]: 1024, // ic10
})
export const icns: Readonly<imageInfoReader> = Object.freeze({
	[isReader]: true,
	min: 4096,
	is: b => b.getUint32(0) in types,
	size(b) {
		const res = [],
			len = b.getUint32(4)
		let off = 8
		do {
			const entype = b.getUint32(off)
			if (!(entype in types)) throw new TypeError('invalid icns, no size retrievable')
			off += b.getUint32(off + 4)

			const size = types[entype]
			res.push([size, size])
			if (off === len) return res
		} while (off < len &&
			off < b.byteLength);
		return res;
	}
} as imageInfoReader)
export default icns
