import { isReader } from './check.mjs';
export const png = Object.freeze({
    [isReader]: true,
    min: 40,
    is: b => b.getUint32(0) === 2303741511
        && b.getUint32(4) === 218765834
        && (b.getUint32(12) === 1229472850
            ? true
            : b.getUint32(12) === 1130840649
                && b.getUint32(28) === 1229472850),
    size: buf => buf.getUint32(12) === 1130840649
        ? [[buf.getUint32(16), buf.getUint32(20)]]
        : [[buf.getUint32(32), buf.getUint32(36)]]
});
export default png;
//# sourceMappingURL=png.mjs.map