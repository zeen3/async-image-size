(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./ico', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.ico = Object.freeze({
        [check_1.isReader]: true,
        min: 2048,
        is(b) {
            const val = b.getUint32(0, true);
            return (65536 === val || 131072 === val);
        },
        size(b) {
            const res = [], num = b.getUint16(4, true);
            let i = 0;
            do {
                const off = 6 + (i << 4);
                res.push([
                    b.getUint8(off) || 256,
                    b.getUint8(off + 1) || 256
                ]);
            } while (++i < num);
            return res;
        }
    });
    exports.default = exports.ico;
});
//# sourceMappingURL=ico.js.map