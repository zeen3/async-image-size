import { isReader } from './check.mjs';
export const dds = Object.freeze({
    [isReader]: true,
    min: 20,
    is: b => b.getUint32(0) === 1145328416,
    size: b => [[
            b.getUint32(12, true),
            b.getUint32(16, true)
        ]]
});
export default dds;
//# sourceMappingURL=dds.mjs.map