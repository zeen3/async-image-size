export const isReader = Symbol('is image info reader');
const mods = new WeakMap();
const getDefault = async (mod) => {
    const mmod = mods.get(mod);
    if (mmod && isReader in mmod)
        return mmod;
    else if (Object.isFrozen(mod.default) && isReader in mod.default) {
        mods.set(mod, mod.default);
        return mod.default;
    }
    throw new TypeError('not an image info reader module');
};
const infos = Object.freeze({
    get tiff() { return import('./tiff.mjs').then(getDefault); },
    get icns() { return import('./icns.mjs').then(getDefault); },
    get webp() { return import('./webp.mjs').then(getDefault); },
    get jpeg() { return import('./jpg.mjs').then(getDefault); },
    get apng() { return import('./png.mjs').then(getDefault); },
    get ktx() { return import('./ktx.mjs').then(getDefault); },
    get dds() { return import('./dds.mjs').then(getDefault); },
    get bmp() { return import('./bmp.mjs').then(getDefault); },
    get ico() { return import('./ico.mjs').then(getDefault); },
    get cur() { return import('./ico.mjs').then(getDefault); },
    get jpg() { return import('./jpg.mjs').then(getDefault); },
    get png() { return import('./png.mjs').then(getDefault); },
    get gif() { return import('./gif.mjs').then(getDefault); },
    get svg() { return import('./svg.mjs').then(getDefault); },
    get eps() { return import('./eps.mjs').then(getDefault); },
});
const imageInfoFirstBytes = Object.freeze({
    get [0]() { return infos.ico; },
    get [2570]() { return infos.svg; },
    get [2620]() { return infos.svg; },
    get [9505]() { return infos.eps; },
    get [15423]() { return infos.svg; },
    get [16973]() { return infos.bmp; },
    get [17476]() { return infos.dds; },
    get [18249]() { return infos.gif; },
    get [18755]() { return infos.icns; },
    get [18761]() { return infos.tiff; },
    get [19789]() { return infos.tiff; },
    get [21065]() { return infos.webp; },
    get [26680]() { return infos.icns; },
    get [26979]() { return infos.icns; },
    get [26984]() { return infos.icns; },
    get [26988]() { return infos.icns; },
    get [26995]() { return infos.icns; },
    get [26996]() { return infos.icns; },
    get [27704]() { return infos.icns; },
    get [29496]() { return infos.icns; },
    get [29752]() { return infos.icns; },
    get [35152]() { return infos.png; },
    get [43851]() { return infos.ktx; },
    get [50640]() { return infos.eps; },
    get [65496]() { return infos.jpg; },
});
export const imageInfoResolver = async (buf, off = 0) => {
    const u16 = new DataView(buf.buffer, buf.byteOffset + off, 2)
        .getUint16(0);
    if (!(u16 in imageInfoFirstBytes))
        throw new SyntaxError('magic bytes not found in first bytes');
    const info = await imageInfoFirstBytes[u16];
    const dv = new DataView(buf.buffer, buf.byteOffset + off, Math.min(info.min, buf.buffer.byteLength - (buf.byteOffset + off)));
    if (info.is(dv))
        return info.size(dv);
    throw new TypeError('magic bytes were incorrect or lacking length');
};
export { imageInfoResolver as default, imageInfoResolver as imageSize, infos as imageContainerInfo, infos, imageInfoFirstBytes, imageInfoFirstBytes as magic, };
//# sourceMappingURL=check.mjs.map