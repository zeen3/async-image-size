(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./text', ["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    let txd;
    exports.txd = txd;
    try {
        if ('TextDecoder' in self)
            exports.txd = txd = new TextDecoder;
    }
    catch (e) {
        exports.txd = txd = ({
            decode(x) {
                return ArrayBuffer.isView(x) ?
                    String.fromCharCode(...new Uint8Array(x.buffer, x.byteOffset, x.byteLength)) :
                    String.fromCharCode(...new Uint8Array(x));
            },
            encoding: 'latin1',
            fatal: false,
            ignoreBOM: false
        });
    }
    ;
});
//# sourceMappingURL=text.js.map