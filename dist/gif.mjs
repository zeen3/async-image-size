import { isReader } from './check.mjs';
export const gif = Object.freeze({
    [isReader]: true,
    min: 10,
    is: b => b.getUint32(0) === 1195984440
        && b.getUint16(4) === 14177
        || b.getUint16(4) === 14689,
    size: b => [[
            b.getUint16(6, true),
            b.getUint16(8, true)
        ]]
});
export default gif;
//# sourceMappingURL=gif.mjs.map