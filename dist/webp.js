(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./webp', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.webp = Object.freeze({
        [check_1.isReader]: true,
        min: 31,
        is: (b) => b.getUint32(0) === 1380533830
            && b.getUint32(8) === 1464156752
            && b.getUint16(12) === 22096
            && b.getUint8(15) === 0x38,
        size(b) {
            switch (b.getUint32(12)) {
                case 1448097880:
                    if (b.getUint8(20) & 0xc1)
                        throw new TypeError('invalid webp, no size retrievable');
                    else
                        return [[
                                1 + ((b.getUint16(25, true) << 8) |
                                    b.getUint8(24)),
                                1 + ((b.getUint16(28, true) << 8) |
                                    b.getUint8(27))
                            ]];
                case 1448097824:
                    if (b.getUint8(20) === 0x2F)
                        throw new TypeError('invalid webp, no size retrievable');
                    else
                        return [[
                                b.getUint16(26, true) & 0x3FFF,
                                b.getUint16(28, true) & 0x3FFF
                            ]];
                case 1448097868:
                    if ((b.getUint32(22) & 0xffffff) === 10289450)
                        throw new TypeError('invalid webp, no size retrievable');
                    else
                        return [[
                                1 + (((b.getUint8(22) & 0x3F) << 8) |
                                    b.getUint8(21)),
                                1 + ((((b.getUint8(24) & 0xF) << 10)) |
                                    (b.getUint8(23) << 2) |
                                    ((b.getUint8(22) & 0xC0) >>> 6))
                            ]];
                default:
                    throw new TypeError('invalid webp, no size retrievable');
            }
        }
    });
    exports.default = exports.webp;
});
//# sourceMappingURL=webp.js.map