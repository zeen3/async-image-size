(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./tiff', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.tiff = Object.freeze({
        [check_1.isReader]: true,
        min: Infinity,
        is(b) {
            const val = b.getUint32(0);
            return val === 1229531648 ||
                val === 1296891946;
        },
        size(b) {
            const le = b.getUint16(0) === 18761, tags = [];
            let off = b.getUint32(4, le);
            if (off > (b.byteLength - 1024))
                off = b.byteLength - off - 8;
            do {
                const code = b.getUint16(0 + off, le), ty = b.getUint16(2 + off, le), ln = b.getUint32(4 + off, le);
                if (code === 0)
                    break;
                if (ln === 1 && (ty === 3 || ty === 4))
                    tags[code] = (b.getUint16(off + 8, le) << 16) |
                        b.getUint16(off + 10, le);
                off += 12;
            } while (off < b.byteLength);
            const w = tags[256], h = tags[257];
            if (w === undefined || h === undefined)
                throw new TypeError('invalid tiff, no size retrievable');
            return [[w, h]];
        }
    });
    exports.default = exports.tiff;
});
//# sourceMappingURL=tiff.js.map