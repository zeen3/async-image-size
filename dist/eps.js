(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./eps', ["require", "exports", "./check", "./text"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    const text_1 = require("./text");
    const gBoundingBox = /^%%BoundingBox:[\t ]*(\d+)[\t ]+(\d+)[\t ]+(\d+)[\t ]+(\d+)[\t ]*$/m;
    exports.eps = Object.freeze({
        [check_1.isReader]: true,
        min: 64,
        is: b => b.getUint32(0) === 622940243 ||
            b.getUint32(0) === 3318797254,
        size(b) {
            const s = text_1.txd.decode(b);
            const bo = s.match(gBoundingBox);
            if (!bo)
                throw new TypeError('Not an encapsulated postscript file holding boundingbox information');
            return [[
                    parseInt(bo[3], 10) - parseInt(bo[1], 10),
                    parseInt(bo[4], 10) - parseInt(bo[2], 10)
                ]];
        }
    });
    exports.default = exports.eps;
});
//# sourceMappingURL=eps.js.map