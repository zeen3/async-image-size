(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./psd', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.psd = Object.freeze({
        [check_1.isReader]: true,
        min: 18,
        is: b => b.getUint32(0) === 943870035,
        size: b => [[
                b.getUint32(18),
                b.getUint32(14)
            ]]
    });
    exports.default = exports.psd;
});
//# sourceMappingURL=psd.js.map