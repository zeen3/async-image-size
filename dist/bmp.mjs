import { isReader } from './check.mjs';
export const bmp = Object.freeze({
    [isReader]: true,
    min: 26,
    is: b => b.getUint16(0) === 16973,
    size: b => [[
            b.getUint32(18, true),
            Math.abs(b.getInt32(22, true))
        ]]
});
export default bmp;
//# sourceMappingURL=bmp.mjs.map