(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./png', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.png = Object.freeze({
        [check_1.isReader]: true,
        min: 40,
        is: b => b.getUint32(0) === 2303741511
            && b.getUint32(4) === 218765834
            && (b.getUint32(12) === 1229472850
                ? true
                : b.getUint32(12) === 1130840649
                    && b.getUint32(28) === 1229472850),
        size: buf => buf.getUint32(12) === 1130840649
            ? [[buf.getUint32(16), buf.getUint32(20)]]
            : [[buf.getUint32(32), buf.getUint32(36)]]
    });
    exports.default = exports.png;
});
//# sourceMappingURL=png.js.map