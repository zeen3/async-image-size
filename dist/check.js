var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./check', ["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    var __syncRequire = typeof module === "object" && typeof module.exports === "object";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.isReader = Symbol('is image info reader');
    const mods = new WeakMap();
    const getDefault = async (mod) => {
        const mmod = mods.get(mod);
        if (mmod && exports.isReader in mmod)
            return mmod;
        else if (Object.isFrozen(mod.default) && exports.isReader in mod.default) {
            mods.set(mod, mod.default);
            return mod.default;
        }
        throw new TypeError('not an image info reader module');
    };
    const infos = Object.freeze({
        get tiff() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./tiff'))) : new Promise((resolve_1, reject_1) => { require(['./tiff'], resolve_1, reject_1); }).then(__importStar)).then(getDefault); },
        get icns() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./icns'))) : new Promise((resolve_2, reject_2) => { require(['./icns'], resolve_2, reject_2); }).then(__importStar)).then(getDefault); },
        get webp() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./webp'))) : new Promise((resolve_3, reject_3) => { require(['./webp'], resolve_3, reject_3); }).then(__importStar)).then(getDefault); },
        get jpeg() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./jpg'))) : new Promise((resolve_4, reject_4) => { require(['./jpg'], resolve_4, reject_4); }).then(__importStar)).then(getDefault); },
        get apng() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./png'))) : new Promise((resolve_5, reject_5) => { require(['./png'], resolve_5, reject_5); }).then(__importStar)).then(getDefault); },
        get ktx() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./ktx'))) : new Promise((resolve_6, reject_6) => { require(['./ktx'], resolve_6, reject_6); }).then(__importStar)).then(getDefault); },
        get dds() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./dds'))) : new Promise((resolve_7, reject_7) => { require(['./dds'], resolve_7, reject_7); }).then(__importStar)).then(getDefault); },
        get bmp() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./bmp'))) : new Promise((resolve_8, reject_8) => { require(['./bmp'], resolve_8, reject_8); }).then(__importStar)).then(getDefault); },
        get ico() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./ico'))) : new Promise((resolve_9, reject_9) => { require(['./ico'], resolve_9, reject_9); }).then(__importStar)).then(getDefault); },
        get cur() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./ico'))) : new Promise((resolve_10, reject_10) => { require(['./ico'], resolve_10, reject_10); }).then(__importStar)).then(getDefault); },
        get jpg() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./jpg'))) : new Promise((resolve_11, reject_11) => { require(['./jpg'], resolve_11, reject_11); }).then(__importStar)).then(getDefault); },
        get png() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./png'))) : new Promise((resolve_12, reject_12) => { require(['./png'], resolve_12, reject_12); }).then(__importStar)).then(getDefault); },
        get gif() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./gif'))) : new Promise((resolve_13, reject_13) => { require(['./gif'], resolve_13, reject_13); }).then(__importStar)).then(getDefault); },
        get svg() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./svg'))) : new Promise((resolve_14, reject_14) => { require(['./svg'], resolve_14, reject_14); }).then(__importStar)).then(getDefault); },
        get eps() { return (__syncRequire ? Promise.resolve().then(() => __importStar(require('./eps'))) : new Promise((resolve_15, reject_15) => { require(['./eps'], resolve_15, reject_15); }).then(__importStar)).then(getDefault); },
    });
    exports.imageContainerInfo = infos;
    exports.infos = infos;
    const imageInfoFirstBytes = Object.freeze({
        get [0]() { return infos.ico; },
        get [2570]() { return infos.svg; },
        get [2620]() { return infos.svg; },
        get [9505]() { return infos.eps; },
        get [15423]() { return infos.svg; },
        get [16973]() { return infos.bmp; },
        get [17476]() { return infos.dds; },
        get [18249]() { return infos.gif; },
        get [18755]() { return infos.icns; },
        get [18761]() { return infos.tiff; },
        get [19789]() { return infos.tiff; },
        get [21065]() { return infos.webp; },
        get [26680]() { return infos.icns; },
        get [26979]() { return infos.icns; },
        get [26984]() { return infos.icns; },
        get [26988]() { return infos.icns; },
        get [26995]() { return infos.icns; },
        get [26996]() { return infos.icns; },
        get [27704]() { return infos.icns; },
        get [29496]() { return infos.icns; },
        get [29752]() { return infos.icns; },
        get [35152]() { return infos.png; },
        get [43851]() { return infos.ktx; },
        get [50640]() { return infos.eps; },
        get [65496]() { return infos.jpg; },
    });
    exports.imageInfoFirstBytes = imageInfoFirstBytes;
    exports.magic = imageInfoFirstBytes;
    exports.imageInfoResolver = async (buf, off = 0) => {
        const u16 = new DataView(buf.buffer, buf.byteOffset + off, 2)
            .getUint16(0);
        if (!(u16 in imageInfoFirstBytes))
            throw new SyntaxError('magic bytes not found in first bytes');
        const info = await imageInfoFirstBytes[u16];
        const dv = new DataView(buf.buffer, buf.byteOffset + off, Math.min(info.min, buf.buffer.byteLength - (buf.byteOffset + off)));
        if (info.is(dv))
            return info.size(dv);
        throw new TypeError('magic bytes were incorrect or lacking length');
    };
    exports.default = exports.imageInfoResolver;
    exports.imageSize = exports.imageInfoResolver;
});
//# sourceMappingURL=check.js.map