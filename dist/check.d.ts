export declare const isReader: unique symbol;
export interface imageInfoReader {
    [isReader]: true;
    readonly min: number;
    is(buf: DataView): boolean;
    size(buf: DataView): ReadonlyArray<ReadonlyArray<number>>;
}
export declare type iirp = Promise<imageInfoReader> | Promise<Readonly<imageInfoReader>>;
export interface imageInfo {
    readonly tiff: iirp;
    readonly icns: iirp;
    readonly webp: iirp;
    readonly jpeg: iirp;
    readonly apng: iirp;
    readonly bmp: iirp;
    readonly dds: iirp;
    readonly ico: iirp;
    readonly cur: iirp;
    readonly jpg: iirp;
    readonly png: iirp;
    readonly gif: iirp;
    readonly svg: iirp;
    readonly ktx: iirp;
    readonly eps: iirp;
    readonly [k: string]: iirp | undefined;
}
declare const infos: Readonly<imageInfo>;
declare const imageInfoFirstBytes: Readonly<{
    readonly [k: number]: iirp;
}>;
export declare const imageInfoResolver: (buf: ArrayBufferView, off?: number) => Promise<ReadonlyArray<ReadonlyArray<number>>>;
export { imageInfoResolver as default, imageInfoResolver as imageSize, infos as imageContainerInfo, infos, imageInfoFirstBytes, imageInfoFirstBytes as magic, };
//# sourceMappingURL=check.d.ts.map