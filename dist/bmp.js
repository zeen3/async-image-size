(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./bmp', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.bmp = Object.freeze({
        [check_1.isReader]: true,
        min: 26,
        is: b => b.getUint16(0) === 16973,
        size: b => [[
                b.getUint32(18, true),
                Math.abs(b.getInt32(22, true))
            ]]
    });
    exports.default = exports.bmp;
});
//# sourceMappingURL=bmp.js.map