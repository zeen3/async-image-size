import { isReader } from './check.mjs';
export const psd = Object.freeze({
    [isReader]: true,
    min: 18,
    is: b => b.getUint32(0) === 943870035,
    size: b => [[
            b.getUint32(18),
            b.getUint32(14)
        ]]
});
export default psd;
//# sourceMappingURL=psd.mjs.map