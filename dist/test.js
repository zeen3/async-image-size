var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./test', ["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    var __syncRequire = typeof module === "object" && typeof module.exports === "object";
    (__syncRequire ? Promise.resolve().then(() => __importStar(require('./check'))) : new Promise((resolve_1, reject_1) => { require(['./check'], resolve_1, reject_1); }).then(__importStar)).then(async (mod) => {
        console.log(mod);
        for (const i in mod.infos)
            console.log(await mod.infos[i]);
        return mod;
    }, console.error);
});
//# sourceMappingURL=test.js.map