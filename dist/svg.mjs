import { isReader } from './check.mjs';
import { txd } from './text.mjs';
const isSVG = /<svg\s([^>"']|"[^"]*"|'[^']*')*>/g;
const gWidth = /\bwidth=(['"])([^%]+?)\1/;
const gHeight = /\bheight=(['"])([^%]+?)\1/;
const gViewBox = /\bviewBox=(['"])\s*\d+(?:\.\d+)?\s+\d+(?:\.\d+)?\s+(\d+(?:\.\d+)?)\s+(\d+(?:\.\d+)?)\s*\1/;
export const svg = Object.freeze({
    [isReader]: true,
    min: 2048,
    is(b) {
        const s = txd.decode(b);
        return isSVG.test(s);
    },
    size(b) {
        const s = txd.decode(b), rt = s.match(isSVG);
        if (rt) {
            const w = rt[0].match(gWidth), h = rt[0].match(gHeight), v = rt[0].match(gViewBox);
            if (w && h && v)
                return [[parseFloat(w[2]), parseFloat(h[2])], [parseFloat(v[2]), parseFloat(v[3])]];
            if (w && h)
                return [[parseFloat(w[2]), parseFloat(h[2])], [NaN, NaN]];
            if (w && v) {
                const V = [parseFloat(v[2]), parseFloat(v[3])];
                const W = parseFloat(w[2]);
                const H = W * (V[1] / V[0]);
                return [[W, H], V];
            }
            if (h && v) {
                const V = [parseFloat(v[2]), parseFloat(v[3])];
                const H = parseFloat(h[2]);
                const W = H * (V[0] / V[1]);
                return [[W, H], V];
            }
            if (v)
                return [[Infinity, Infinity], [parseFloat(v[2]), parseFloat(v[3])]];
        }
        throw new TypeError('invalid svg, no size retrievable');
    }
});
export default svg;
//# sourceMappingURL=svg.mjs.map