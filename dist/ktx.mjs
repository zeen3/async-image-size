import { isReader } from './check.mjs';
export const ktx = Object.freeze({
    [isReader]: true,
    min: 48,
    is: b => b.getUint32(0) === 2873840728 &&
        b.getUint32(4) === 540094907 &&
        b.getUint32(8) === 218765834,
    size(b) {
        const le = b.getUint32(12) === 16909060;
        return [[
                b.getUint32(40, le),
                b.getUint32(44, le),
                b.getUint32(48, le)
            ]];
    }
});
export default ktx;
//# sourceMappingURL=ktx.mjs.map