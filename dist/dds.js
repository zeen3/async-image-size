(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./dds', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.dds = Object.freeze({
        [check_1.isReader]: true,
        min: 20,
        is: b => b.getUint32(0) === 1145328416,
        size: b => [[
                b.getUint32(12, true),
                b.getUint32(16, true)
            ]]
    });
    exports.default = exports.dds;
});
//# sourceMappingURL=dds.js.map