(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./ktx', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.ktx = Object.freeze({
        [check_1.isReader]: true,
        min: 48,
        is: b => b.getUint32(0) === 2873840728 &&
            b.getUint32(4) === 540094907 &&
            b.getUint32(8) === 218765834,
        size(b) {
            const le = b.getUint32(12) === 16909060;
            return [[
                    b.getUint32(40, le),
                    b.getUint32(44, le),
                    b.getUint32(48, le)
                ]];
        }
    });
    exports.default = exports.ktx;
});
//# sourceMappingURL=ktx.js.map