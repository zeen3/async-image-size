(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./icns', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    const types = Object.freeze({
        [1229147982]: 32,
        [1229147683]: 32,
        [1768123683]: 16,
        [1768123700]: 16,
        [1768123704]: 16,
        [1768125219]: 16,
        [1768125236]: 16,
        [1768125240]: 16,
        [1769157426]: 16,
        [1933077867]: 16,
        [1768124468]: 16,
        [1768123444]: 32,
        [1768123448]: 32,
        [1768698674]: 32,
        [1815637355]: 32,
        [1768124469]: 32,
        [1768108337]: 32,
        [1768122420]: 48,
        [1768122424]: 48,
        [1768436530]: 48,
        [1748528491]: 48,
        [1768124470]: 64,
        [1768108338]: 32,
        [1769222962]: 128,
        [1949855083]: 128,
        [1768108087]: 128,
        [1768108088]: 256,
        [1768108339]: 256,
        [1768108089]: 512,
        [1768108340]: 512,
        [1768108336]: 1024,
    });
    exports.icns = Object.freeze({
        [check_1.isReader]: true,
        min: 4096,
        is: b => b.getUint32(0) in types,
        size(b) {
            const res = [], len = b.getUint32(4);
            let off = 8;
            do {
                const entype = b.getUint32(off);
                if (!(entype in types))
                    throw new TypeError('invalid icns, no size retrievable');
                off += b.getUint32(off + 4);
                const size = types[entype];
                res.push([size, size]);
                if (off === len)
                    return res;
            } while (off < len &&
                off < b.byteLength);
            return res;
        }
    });
    exports.default = exports.icns;
});
//# sourceMappingURL=icns.js.map