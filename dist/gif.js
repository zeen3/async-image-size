(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('./gif', ["require", "exports", "./check"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const check_1 = require("./check");
    exports.gif = Object.freeze({
        [check_1.isReader]: true,
        min: 10,
        is: b => b.getUint32(0) === 1195984440
            && b.getUint16(4) === 14177
            || b.getUint16(4) === 14689,
        size: b => [[
                b.getUint16(6, true),
                b.getUint16(8, true)
            ]]
    });
    exports.default = exports.gif;
});
//# sourceMappingURL=gif.js.map