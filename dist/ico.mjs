import { isReader } from './check.mjs';
export const ico = Object.freeze({
    [isReader]: true,
    min: 2048,
    is(b) {
        const val = b.getUint32(0, true);
        return (65536 === val || 131072 === val);
    },
    size(b) {
        const res = [], num = b.getUint16(4, true);
        let i = 0;
        do {
            const off = 6 + (i << 4);
            res.push([
                b.getUint8(off) || 256,
                b.getUint8(off + 1) || 256
            ]);
        } while (++i < num);
        return res;
    }
});
export default ico;
//# sourceMappingURL=ico.mjs.map